---
title: Nine Summer of Code Students Starting Work on Calligra
date: 2012-04-26
author: boudewijn
categories: []
---

This summer, Google's Summer of Code is bigger than ever. Sixty students are ready to start work under the umbrella of the KDE project, and out of those sixty, eight are going to work on Calligra. We have some newcomers and some old hands. Let's introduce the projects!

### Presentation Animations

While Calligra Stage already supports animating the graphical objects on a slide, it is lacking a good and user-friendly tool to create those animations. Paul Mendez from Ecuador is going to work on this task. Shapes will be put in a time-line with actions defined for every step, as shown in this mock-up he drafted for his application:

![](https://lh6.googleusercontent.com/KtBkJWlqy9CZmnqDELGX-Fr7wNF_IlcZ0IsCQKCZPyzLy8eFFLUSKYLe5_2mGSVsxmb7zxpiwMupN572Wcjd49KbbV2TtZJ83RRgaguG2zozSt-qL5o)

### Bibliographies and Citations for Calligra Words

Handling bibliographies and citations is currently quite primitive and unfinished in Calligra Words, but it's an important feature for an application that at one point might have been named "Calligra Papers" -- students writing papers are one of the target audiences. Calligra Words developer Smit Patel will create a system to store and edit citations and then interface this system with external bibliography engines like Kile and Bibus.

### More and better OpenFormula Support for Calligra Sheets

Calligra Sheets supports the OpenFormula specification to implement formula functions -- but not yet at the highest level! Nityam Vakil will undertake the task to get us there, by implementing the missing 18 functions. And writing unit tests for them, of course.

### Pivot Tables!

A  feature much-requested by users, Pivot Tables, is going to be worked on by Jigar Raisinghani. Pivot tables help users to make sense of large amounts of data, and the Calligra Sheets developers have been looking into ways of implementing this feature for a long time.

### Saving Charts to OpenDocument

In the past year or two, Calligra has made huge steps improving our loading and display of charts from OpenDocument and OOXML documents. But saving has lagged rather badly... And that's a situation that obviously won't do! So Brijesh Patel will spend his summer improving the issue. In fact, he has already started writing patches for saving pie and ring charts!

### Mathematical Formulas

KOffice 1.6 had pretty good support for showing and editing mathematical formulas, but support has had a bumpy road in the transition to Qt4 and the formula component could be so much better... Enter Abishek, who is not afraid of a really tough job. He will improve editing of formulas, but also the rendering quality.

### Krita

The other three students will be working on Krita. Head over to [krita.org](http://krita.org/item/110-summer-of-code-2012) to see what they will be working on!
