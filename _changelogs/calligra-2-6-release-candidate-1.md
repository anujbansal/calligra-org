---
title: Calligra 2.6 Release Candidate 1
date: 2012-12-05
author: Inge Wallin
categories: []
---

The Calligra team is proud to announce the first release candidate of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). Since show stopper bugs were discovered after the tagging we expect one more release candidate before the actual release. Compared to the beta, this new release contains a number of bug fixes.

## Try It Out

The source code of this version is available for download here: [calligra-2.5.92.tar.bz2](http://download.kde.org/unstable/calligra-2.5.92/calligra-2.5.92.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

One of our developers is experimenting with packaging the Calligra Suite for Mac OS X and the final release may have working packages.

## About the Calligra Suite

Calligra is a KDE community project. Learn more at [http://www.calligra.org/](http://www.calligra.org/).
