---
title: Calligra 2.5.2 Released
date: 2012-09-13
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.5.2, the second bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.5.1 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.5.2. There are several others that are not mentioned here:

### Sheets:

- Fixed bug that prevented plugins from showing on Settings->Configure Sheets...->Plugins page (bug 291343).)

### Stage:

- Fixed display of shapes in the document docker.
- Fix unwanted offset after cancel slideshow (bug 306118)

### Kexi:

- Fixed bug in Query Designer: removed SQL Editor's history as it was not functional (bug 306145)
- Make string concatenation operator || work in Query Designer (bug 305793)
- Startup GUI: fetch startup UI definition from x.y.0 URL for any x.y.z version, not from x.y.z URL
- Buildsystem: make it possible to disable database drivers even if their dependencies are present and found (wish 305683)

### Common:

- Fix resizing of horizontal and vertical lines (bug 306133).
- Fix reordering of shapes (bug 306309)
- Fix some icons for the formula shape.
- Trigger repaint of the picture shape after insertion (bug 305548)
- Fix some corner cases when loading borders.

## Try It Out

The source code of this version is available for download here: [calligra-2.5.2.tar.bz2](http://master.kde.org/stable/calligra-2.5.2/calligra-2.5.2.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build and test the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
