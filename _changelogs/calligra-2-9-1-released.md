---
title: Calligra 2.9.1 Released
date: 2015-03-15
author: cyrilleberger
categories: [announcements,stable]
---

The Calligra team has released version 2.9.1, the first of the bugfix releases of the [Calligra Suite](http://www.calligra.org/) in the 2.9 series. This release contains a few important bug fixes to 2.9.0 and we recommend everybody to update.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-1-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### Kexi

- _General_:
    - Show clear buttons in line edits of the Object Name dialogs.
    - Use consistent style of title naming in window tabs.
    - Fix status bar online updates when e.g. /tmp is on different partition than /home.
    - Fix crash on backward search (bug 337015)
- _Database handling_:
    - Fix compacting of .kexi files when e.g. /tmp is on different partition than /home.
- _Tables_:
    - Show "No field selected" hint on first use of an empty Kexi table designer.
- _Queries_:
    - Make SUBSTR(X, Y \[, Z\]) built-in SQL function work (bug 333206)
    - Remember and restore dirty flag equal to false while changing modes, e.g. this fixes the flag state when the query designer is opened in SQL view and the visual view fails to open.
- _Reports_:
    - Map theme can be picked from the property editor
    - Add keyboard shortcuts for copy/paste/cut/delete actions (bug 334967)
    - Cut action should make the report design dirty.

### Krita

- Fix the outline cursor on CentOS 6.5
- Update G'Mic to the very latest version (but the problems on Windows are still not resolved)
- Improve the layout of the filter layer/⁠mask dialog's filter selector
- Fix the layout of the pattern selector for fill layers
- Remove the dependency of QtUiTools, which means that no matter whether the version Qt installed and the version Qt Krita was built agast differ, the layer metadata editors work
- Fix a bug that happened when switching between workspaces
- Fix bug 339357: the time dynamic didn't start reliably
- Fix bug 344862: a crash when opening a new view with a tablet stylus
- Fix bug 344884: a crash when selecting too small a scale for a brush texture
- Fix bug 344790: don't crash when resizing a brush while drawing
- Fix setting the toolbox to only one icon wide
- Fix bug 344478: random crash when using liquify
- Fix bug 344346: Fix artefacts in fill layers when too many parallel updates happened
- Fix bug 184746: merging two vector layers now creates a vector layer instead of rendering the vectors to pixels
- Add an option to disable the on-canvas notification messages (for some people, they slow down drawing)
- Fix bug 344243: make the preset editor visible in all circumstances

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.1.tar.xz](http://download.kde.org/stable/calligra-2.9.1/calligra-2.9.1.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.1/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.1/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

We have approached the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan). The next step, Calligra 3.0, will be based on new technologies. Expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
