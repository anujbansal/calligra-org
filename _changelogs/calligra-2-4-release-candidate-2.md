---
title: Calligra 2.4 Release Candidate 2
date: 2012-03-21
author: Inge Wallin
categories: [announcements]
---

The Calligra team is proud to announce the second release candidate of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). This is the beginning of the end of a very long journey that started over a year ago and will very soon lead up to the first stable release of Calligra. The watchful reader may notice that there never was a release candidate 1. The reason for that is that the team found some serious errors that had to be fixed.

Name Change

Due to a name clash with another spreadsheet program the application formerly known as Calligra Tables is now called Calligra Sheets.

News in This Release

There is now a hard feature freeze and no more new features are allowed into Calligra. Therefore all changes since the last beta version are bugfixes. The focus of this release has been to continue on **improvements of saving and roundtripping** and a few **crash fixes**.

Other than saving, one bad set of **issues with inheritance of styles** was fixed in this release and of course the usual amount of crashes and normal bugs are fixed. And also as usual the team is grateful for all bug reports in this and other areas.

The mobile user interface **Calligra Active** is not affected by the freeze and has two new features: **Selection and Copy for text** and a **Custom Slideshow** feature. Calligra Active was also featured in an [article on Linux Weekly News](http://lwn.net/Articles/476955/ "LWN: Porting office suites to mobile platforms") about mobile versions of free office suites. It's well worth a read. (For people reading this on February 1st or earlier the article is for subscribers only but it will be made available for the public on February 2nd.)

## Try It Out

The source code of this version is available for download here: [calligra-2.3.92.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.92/calligra-2.3.92.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

Calligra is a KDE community project. Learn more at [http://www.calligra.org/](http://www.calligra.org/).
