---
title: Calligra 2.8 Beta Released
date: 2013-12-08
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the first beta release of version 2.8 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite") for testing! The team will now focus on fixing remaining bugs. Let's make sure the final release of 2.8, expected by the end of January is as stable as possible by giving the current beta a good testing!

## News in This Release

Calligra 2.8 will contain many new features. Here is an overview of the most important ones:

**Words,** the word processing application, now has support for comments in the text.

**Author,** the writer's application, has the same enhancements as Words.

**Sheets,** the spreadsheet application, has improved support for pivot tables.

In **Kexi**, the visual database creator, there is a new feature to open hyperlinks in forms. But the big news is that Kexi is now possible to build on Windows which was not the case before in 2.x series.

There are some improvements in the general **shapes** available from most Calligra applications: You can now paste any shape from any application into any application. There are also improvements in copy and paste of images and rich text.

**Krita,** the full-featured free digital painting studio for artists will have a very exciting release! There are many new features, among them a new high-quality scaling mode for the OpenGL canvas, [G'Mic](http://http://gmic.sourceforge.net/) integration, a wraparound painting mode for easy creation of textures and tiles, support for touch screens. There are two new application in this release, as well: Krita Sketch, aimed at tablets and Krita Gemini, which can switch between desktop and tablet mode at the press of a button. The full set of improvements is huge! Take a look at [the preview](http://krita.org/item/203-on-the-road-to-krita-2-8). Krita 2.8 will also be available for Windows.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.7.90.tar.xz](http://download.kde.org/unstable/calligra-2.7.90/calligra-2.7.90.tar.xz). Alternatively, you can [download binaries for many Linux distributions](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
