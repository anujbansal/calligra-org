---
title: Calligra 2.8 Beta 2 Released
date: 2014-01-22
author: cyrilleberger
categories: []
---

The Calligra team is proud and pleased to announce the second beta release of version 2.8 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite") for testing! The team will now focus on fixing remaining bugs. Let's make sure the final release of 2.8, expected by the end of January is as stable as possible by giving the current beta a good testing!

## Try It Out

The source code of the snapshot is available for download: [calligra-2.7.91.tar.xz](http://download.kde.org/unstable/calligra-2.7.91/calligra-2.7.91.tar.xz). Alternatively, you can [download binaries for many Linux distributions](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
