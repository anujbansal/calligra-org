---
title: Calligra Suite 2.4 Beta 5
date: 2011-12-15
author: cyrilleberger
categories: []
---

The Calligra team has decided to release a fifth beta version of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). There were originally going to be three beta versions followed by one or more release candidates. However in the last week the team has discovered a number of serious bugs, particularly in the area of style handling and style inheritance. The release candidates will therefore be delayed and there will likely also be further beta version before the first release candidate.

This fifth beta is ongoing work to fix the newly discovered and older bugs.

## Try It Out

The source code of this version is available for download here: [calligra-2.3.85.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.85/calligra-2.3.85.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
