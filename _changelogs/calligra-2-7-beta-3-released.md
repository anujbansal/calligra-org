---
title: Calligra 2.7 Beta 3 Released
date: 2013-06-12
author: Inge Wallin
categories: []
---

The Calligra team announces the third beta release of version 2.7 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). This release is a bugfix release in preparation for the final release of Calligra 2.7 in June.

## Updates in This Release

Calligra 2.7 beta3 has a number of important fixes from the second beta. Here is an excerpt of the most important ones.

A fix in the **common code** that makes calligra not hang on some faulty documents, and fixes that make it work better on ARM.

**Words,** the word processing application, has a fix in the WordPerfect import, which makes the filter work (and being enabled) again.

**Krita,** the 2D paint application, has an updated splash screen, a fix for the file layer (#320017), a fix for alpha color space conversion, a fix for merging layers with inherited alpha (#318318), a fix in the layout of tool option panels, an update of the scRGB profile (#319579), an improvement in the transformation while rotating (#319444), and several crash fixes (#319764, #319855, #320037).

## Try It Out

The source code of this release is available for download: [calligra-2.6.92.tar.bz2](http://download.kde.org/unstable/calligra-2.6.92/calligra-2.6.92.tar.bz2 ). Alternatively, you can [download binaries for many Linux distributions and windows](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
