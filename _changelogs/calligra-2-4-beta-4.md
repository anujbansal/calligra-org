---
title: Calligra 2.4 Beta 4
date: 2011-11-23
author: Inge Wallin
categories: [announcements]
---

The Calligra team has decided to release a fourth beta version of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). There were originally going to be three beta versions followed by one or more release candidates. However in the last week the team has discovered a number of serious bugs, particularly in the area of style handling and style inheritance. The release candidates will therefore be delayed and there will likely also be a fifth beta version before the first release candidate.

This fourth beta is ongoing work to fix the newly discovered and older bugs.  You can see the [full list of changes](http://www.calligra.org/changelogs/calligra-2-4-beta-4-changelog/ "Changelog for Calligra 2.4 beta 4") for more details.

News in This Release

The highlights of this release include:

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.4 is the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor, has improvements for image anchoring.

**Tables**, the spreadsheet application, has improvements for background colors in some instances as well as performance enhancements while loading.

**Kexi**, the visual database creator, has received stability fixes and finalization of the Global Search.

**Plan**, the project management application**,** has several improvements, among them performance views and remembering user settings better.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the painting application, has a many bugfixes. One noteworthy thing is improvements in CMYK handling including loading from PSD.

Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the last beta release. Here are a few of them. For full details, see again the list of changes.

- Some crash fixes
- Improvements in text layout.
- Improvements in chart legends
- Saving of GIF images work better now
- Quadratic Bezier curves save correctly now.
- Improvements in conversions from Microsoft formats, especially DOCX, XLSX and PPTX.

The native file format of Calligra is the Open Document Format, ODF. Compatibility with other suites and formats has always been a priority and as usual all the import filters for Microsoft formats have received attention and improvements. At this time, the Calligra import filters for docx/xlsx/pptx are the best free translations tools available anywhere.

## Try It Out

The source code of this version is available for download here: [calligra-2.3.84.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.84/calligra-2.3.84.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
