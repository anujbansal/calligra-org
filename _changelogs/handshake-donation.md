---
title: Sizeable donation from Handshake Foundation
date: 2018-10-15
author: jstaniek
categories: []
---

We're glad to announce that we received donation of 100,000 USD, which is part of 300,000 USD offered to our KDE organization. Quite appropriate for a birthday present, as the KDE project just turned 22 this last weekend! It's true recognition for KDE as one of the world's largest open source project.

More information: [KDE e.V. receives a sizeable donation from Handshake Foundation](https://dot.kde.org/2018/10/15/kde-ev-receives-sizeable-donation-handshake-foundation).

[![Handshake](/assets/img/hi-2292499_1280_trimmed.jpg)](/assets/img/hi-2292499_1280_trimmed.jpg)
