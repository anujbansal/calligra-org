---
title: Calligra 2.9 Beta 3 Released
date: 2015-02-13
author: jstaniek
categories: []
---

We’re pleased to announce the **last Calligra 2.9 beta release** for you to test. We are as good at fixing issues as your [reports](http://bugs.kde.org/) are so please keep up the good work.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-beta-3-released/#support)

**The next release [expected](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan#Stable_releases) on February 26th will be the final 2.9.0!** Please ask your favourite Linux vendor about the plans for packages.

## New Features and Improvements in This Release

### General

- Fix a crash when applying changes with the style manager.
- The drop caps themselves should not be subjected to line spacing rules, only the actual lines of text should be. Crash fixed. (bug [342185](https://bugs.kde.org/show_bug.cgi?id=342185))
- The layout of spell checking markup should be invalidated whenever we have layouted a block.
- Remove inline objects from manager when Delete command is executed. (bug [303492](https://bugs.kde.org/show_bug.cgi?id=303492))
- Make text flowing around shapes with shadow. (bug [335784](https://bugs.kde.org/show_bug.cgi?id=335784))
- EMF: Legend of the chart in a PPT document is displaying ugly. (bug [271016](https://bugs.kde.org/show_bug.cgi?id=271016))
- Make sure the result of finding text can't be extended when writing after the text. (bug [292410](https://bugs.kde.org/show_bug.cgi?id=292410))
- Assign standard shortcuts for text alignment Ctrl+L, Ctrl+E, Ctrl+R. We had to reassign the shortcut for find and replace too, so now: Ctrl+H (also standard in word processing apps). Also set Ctrl+shift+F for distraction free mode. (bug [303739](https://bugs.kde.org/show_bug.cgi?id=303739))
- Fix some crashes in change tracking. (bug [334150](https://bugs.kde.org/show_bug.cgi?id=334150))
- SVM: Underline effect and italics were not retained. (bug [275322](https://bugs.kde.org/show_bug.cgi?id=275322))
- SVG file was wrongly placed. (bug [322377](https://bugs.kde.org/show_bug.cgi?id=322377))
- Fix undo/redo for text ranges. (bug [342869](https://bugs.kde.org/show_bug.cgi?id=342869))
- Make sure we don't delete anchors if we are just deleting adjacent characters. (bug [342867](https://bugs.kde.org/show_bug.cgi?id=342867))
- File dialogs: Fix all-supported formats on GNOME.
- File dialogs: Restore the All Formats option.
- Fix for new tags not loading after application restart. (bug [342541](https://bugs.kde.org/show_bug.cgi?id=342541))

### Kexi - Visual Database Applications Builder

Tabular views received many more improvements.

- General:
    - Fix (reverse) logic in 3 'dont ask' settings: dontAskBeforeDeleteRow is now called AskBeforeDeleteRow, dontShowWarningsRelatedToPluginsLoading is now called ShowWarningsRelatedToPluginsLoading, dontAskBeforeDeleteConnection is now called AskBeforeDeleteConnection (Thanks to [Robert Leleu](https://forum.kde.org/viewtopic.php?f=221&t=124845)).
    - Delete no longer working or needed --skip-startup-dialog command line option. (bug [338247](https://bugs.kde.org/show_bug.cgi?id=338247))
    - Support recent naming changes in the "Breeze" icon theme.
    - Support "Breeze Dark" theme too.
- Tables:
    - Always display indicator for autonumber in the insert row. Before it was displayed only in the current row. [Now it's consistent with how default values are displayed](http://kexi-project.org/pics/2.9/kexi-2.9-autonumber-and-default-values.png).
    - Designer: check duplicates and empty design before proposing PK. This makes the process of creating designed table more logical from the user's perspective.
    - Designer: make the last column (Comments) stretched. (minor defect [#7](https://community.kde.org/Kexi/Porting_to_Qt%26KDE_4#Table_View))
    - Do not paint headers outside of the table view, over the area of scrollbars. (minor defect [#3](https://community.kde.org/Kexi/Porting_to_Qt%26KDE_4#Table_View))
    - Fix behaviour on column width change: ensure column is visible after interactive adjusting width of this column to the contents by double mouse click. However, don't move cell cursor there.
    - Clicking "Go to new record" in tableview moves to first reasonable column. That means first column that has no autoincrement flag set. Before it was the active column. (usability fix)
- Queries:
    - Set Ctrl+F5 shortcut for "Check Query" of the SQL query designer. (bug [338834](https://bugs.kde.org/show_bug.cgi?id=338834))
- Forms:
    - Update geometry value in property editor when resize handles are moved (in real time).
    - Do not display inline editor when re-doing "Insert widget" commands (usability fix).
    - Don't treat undoing changes to color properties as new commands so now undo/redo works as expected.
    - Fix crash during elements deletion in forms. (bug [339251](https://bugs.kde.org/show_bug.cgi?id=339251))
    - Fix combo box selection after changing the data source. (bug [278299](https://bugs.kde.org/show_bug.cgi?id=278299))
    - Fix updating area of horizontal scrollbar when it gets hidden and gtk+ widget style is used.

### Krita - Digital Painting for Artists

- New splash screen by Tyson Tan!
- Fix noisy complaints from libpng about nothing
- Hide the next/previous blending mode, snap-to-grid and reload-file actions because they don't work
- Fix the shortcuts for setting brush opacity
- Fix inverted softness (bug [342747](https://bugs.kde.org/show_bug.cgi?id=342747))
- Fix ghost pixels on group layers with no children (bug [331554](https://bugs.kde.org/show_bug.cgi?id=331554))
- Fix opacity setting for pattern fill layers
- G'Mic: Add progress reporting for small previews
- G'Mic: Cancel now stops execution of slow filters
- G'Mic: Don't crash when closing the G'Mic dialog after doing nothing
- G'Mic; fix url for updates
- Fix issues with Genius Tablets (bug [342641](https://bugs.kde.org/show_bug.cgi?id=342641))
- Fix painting on selection masks
- Make the palettes docker follow the general background color setting
- Add a temporary dialog to fix issues when the desktop resolution and the wintab resolution don't match up
- Fix crash when using the color transfer filter (bug [342287](https://bugs.kde.org/show_bug.cgi?id=342287))
- Fix KToolLine to handle end and cancel requests (bug [336959](https://bugs.kde.org/show_bug.cgi?id=336959))
- Fix a bunch of menu options to only be active at the right moment
- Add unit of measurement to offset image dialog
- Fix initialization of the crop tool (bug [342842](https://bugs.kde.org/show_bug.cgi?id=342842))
- Improve default values for the crop tool (bug [242844](https://bugs.kde.org/show_bug.cgi?id=242844))
- Fix crash in shaped gradient with shaped smaller than 3px wide (bug [342942](https://bugs.kde.org/show_bug.cgi?id=342942))
- Show open/save buttons in the ruler assistant tool on Windows (bug [342348](https://bugs.kde.org/show_bug.cgi?id=342348))
- Add auto-leveling to the adjust/levels filter (Aleksander Demko's first patch!)
- Don't push the Copy action on the undo stack (bug [343328](https://bugs.kde.org/show_bug.cgi?id=343328))
- Remember the constrain proportions settings in the canvas size dialog (bug [343282](https://bugs.kde.org/show_bug.cgi?id=343282))
- Fix spacing of rotating brushes (bug [329026](https://bugs.kde.org/show_bug.cgi?id=329026))
- Fix crash when selecting the texture option for the pixel brush engine (bug [342749](https://bugs.kde.org/show_bug.cgi?id=342749))
- Make the "on hover" layer thumbnail configurable (bug [342168](https://bugs.kde.org/show_bug.cgi?id=342168))
- Fix an issue where you'd have to press cancel multiple times to close Krita (bug [343070](https://bugs.kde.org/show_bug.cgi?id=343070))
- Notify the user when copying an empty selection (bug [343092](https://bugs.kde.org/show_bug.cgi?id=343092))
- Expand the color picker tool to be able to use a radius up to 900 pixels (bug [337406](https://bugs.kde.org/show_bug.cgi?id=337406))
- Don't crash if Krita's settings has an active preset that no longer exists (bug [340229](https://bugs.kde.org/show_bug.cgi?id=340229))
- Don't crash when closing Krita while the reference image docker is still loading thumbnails (bug [342896](https://bugs.kde.org/show_bug.cgi?id=342896))
- Switch to an appropriate tool when switching between pixel and vector layers (bug [335092](https://bugs.kde.org/show_bug.cgi?id=335092))
- Support indirect painting mode for masks (bug [318882](https://bugs.kde.org/show_bug.cgi?id=318882))
- Don't crash when merging selected layers (bug [343540](https://bugs.kde.org/show_bug.cgi?id=343540))
- Bring back the undo docker's preview thumbnails (bug [277884](https://bugs.kde.org/show_bug.cgi?id=277884))
- Don't crash when undoing points in polyline stroke or selection (bug [342921](https://bugs.kde.org/show_bug.cgi?id=342921))
- Make shift-z undo points in all poly tools (bug [342921](https://bugs.kde.org/show_bug.cgi?id=342921))
- Make the shortcut for undoing poly tool points configurable (bug [342919](https://bugs.kde.org/show_bug.cgi?id=342919))
- Disable the arrow keys for panning the canvas by default (bug [342023](https://bugs.kde.org/show_bug.cgi?id=342023))
- Update the minimum zoom level after scaling the image (bug [342709](https://bugs.kde.org/show_bug.cgi?id=342709))
- Rearrange the settings menu to be more logical (bug [342068](https://bugs.kde.org/show_bug.cgi?id=342068))
- Only lock the tools if the layer is invisible, but allow moving and deleting of invisible layers (bug [337912](https://bugs.kde.org/show_bug.cgi?id=337912))
- Fix issues when working with the color selectors in HDR mode (bug [343531](https://bugs.kde.org/show_bug.cgi?id=343531))
- Don't save the crop tool's force ratio setting (bug [343287](https://bugs.kde.org/show_bug.cgi?id=343287))
- Fix cyclic updates of the currently selected color (bug [343531](https://bugs.kde.org/show_bug.cgi?id=343531))
- G'Mic: Don't crash when enabling the small preview in interactive colorize (bug [343616](https://bugs.kde.org/show_bug.cgi?id=343616))
- Fix a crash on loading an image (bug [340752](https://bugs.kde.org/show_bug.cgi?id=340752))
- G'Mic: increase the stack size to ridiculous proportions on Windows so the parser doesn't crash
- G'Mic: start supporting interactive colorize on Windows (still not done, needs adding support for pthreads)
- Follow the settings for visibillity of the scrollbars (bug [342217](https://bugs.kde.org/show_bug.cgi?id=342217))
- Fix shaped gradients for selections with holes (bug [343187](https://bugs.kde.org/show_bug.cgi?id=343187))
- Fix floodfill for 16 bits integer/channel RGB images (bug [343365](https://bugs.kde.org/show_bug.cgi?id=343365))
- Fix the zoom level of the scratchpad
- Fix a big slowdown in the layer properties dialog with big layers and big images (bug [343685](https://bugs.kde.org/show_bug.cgi?id=343685))
- Fix file layer position resetting
- Add the recent documents to the list in the new images dialog (bug [340949](https://bugs.kde.org/show_bug.cgi?id=340949))
- Fix support for Wacom Airbrush devices. Patch by Arturg. (bug [343545](https://bugs.kde.org/show_bug.cgi?id=343545))
- Make the text brush load and display values from the current brush
- Fix the text brush when rotation is set to drawing angle (bug [330185](https://bugs.kde.org/show_bug.cgi?id=330185))
- Fix recognizing the bamboo stylus (bug [343545](https://bugs.kde.org/show_bug.cgi?id=343545))
- Don't deadlock when loading a fill layer (bug [343734](https://bugs.kde.org/show_bug.cgi?id=343734))
- Don't assert when checking the texture option (bug [343837](https://bugs.kde.org/show_bug.cgi?id=343837))
- Make it possible again to select an image in the color transfer filter on Windows (bug [343706](https://bugs.kde.org/show_bug.cgi?id=343706))
- G'Mic: fix a crash when browsing through the filters
- Update the layer thumbnails in the layerbox after every stroke, instead of when hovering over the layerbox (bug [343699](https://bugs.kde.org/show_bug.cgi?id=343699))

### Calligra Sheets - The Spreadsheet

- Usability: when double-clicking outside of any shape, switch to the cell tool.
- Fix drag and drop of cell values and selections.
- When selecting a range, display its dimensions in the status bar.
- When saving cell values, take formatting into account when determining value type. This fixes some load/save issues.

### Calligra Words - The Word Processor

- Page layout of footer that is larger than the two times bottom page margin should not disappear from the first page. (bug [275288](https://bugs.kde.org/show_bug.cgi?id=275288))
- Page number variable was updated way too often into inconsistent values. Now it is less frequentl changed. (bug [260084](https://bugs.kde.org/show_bug.cgi?id=260084))
- Fix loading text shapes with flowing text in weird order. We basically didn't expect such a weird order, but now we do a two step loading, first recording the order, and only then actually placing the text flow in that order. (bug [327565](https://bugs.kde.org/show_bug.cgi?id=327565))
- Fix layout bug that basically put all page anchored shapes referring to page content's rect on the first page even if the shape was supposed to be on any other page.
- Make it possible to insert a textshape again (without a crash)
- Fix crash when ungrouping group. There is still some strange behaviour. (bug [260088](https://bugs.kde.org/show_bug.cgi?id=260088))
- Disallow shape properties action for auto-generated frames.
- Show the display name of the current page style in the status bar. (bug [293549](https://bugs.kde.org/show_bug.cgi?id=293549))
- Make sure annotations are show when loading a new file with annotations. Make sure we also apply the runaround properties to the shape on first redo. (bug [329301](https://bugs.kde.org/show_bug.cgi?id=329301))
- Bibliography and table of contents are not stored in text frame, so don't move out of it. (bug [286667](286667))

## Try It Out

- The source code is available for download: [calligra-2.8.92.tar.xz](http://download.kde.org/unstable/calligra-2.8.92/calligra-2.8.92.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## What's Next and How to Help?

We're approaching the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) to be released in February 2015. It will be followed by Calligra 3.0 based on new technologies later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita) forums. Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to:

- Support entire Calligra indirectly by donating to KDE, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations),
- Support Krita directly by donating to the Krita Foundation, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/),
- Support Kexi directly by donating to its current BountySource fundraiser, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About Calligra

Calligra Suite is a graphic art and office suite developed by the [KDE community](https://www.kde.org). It is available for desktop PCs, tablet computers, and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics, and digital painting. See more information at the website [http://www.calligra.org](http://www.calligra.org/).

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
