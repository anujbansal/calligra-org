---
title: dot.kde.org reports on the first Calligra sprint
date: 2011-04-16
author: boudewijn
categories: []
---

Wondering what happened at the first Calligra sprint in Berlin? Read all about it on [dot.kde.org](http://dot.kde.org/2011/04/15/first-calligra-sprint "First Calligra Sprint in Berlin")! Short summary: it rocked!
