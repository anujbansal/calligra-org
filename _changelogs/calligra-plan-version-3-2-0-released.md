---
title: Calligra Plan version 3.2.0 released
date: 2019-11-18
author: danders
categories: [announcements,stable]
---

We are pleased to announce the relase of Calligra Plan 3.2.0. 
Tarball can be found here:

[http://download.kde.org/stable/calligra/3.2.0/calligraplan-3.2.0.tar.xz.mirrorlist](http://download.kde.org/stable/calligra/3.2.0/calligraplan-3.2.0.tar.xz.mirrorlist)

Summary of changes:

General:  
\* Add drag/drop and copy/paste using text/html and text/plain mimetypes.  
This can be done from most table based views along with gantt charts and performance charts.

\* BUG: 412063 - landscape printing/export is not possible.

\* Add project templates.  
Templates can be created from regular projects and stored in different places.  
Settings are added to point to template storage.

\* BUG: 412325 - calligraplan doesn't load translations from calligraplanlibs.mo.

\* Add hyper link handling to description editor.

\* Use project name in recent projects list instead of file name.

\* Collect project settings into separate Project menu.  
This makes the ui more descriptive and intuitive.

\* Add view options to the View menu.

\* Open description from treeviews on doubleclick.

\* Improve edit/view Documents.  
Open documents dialog from context menu in all relevant views.  
Enable opening url from documents dialog.

\* Add documents to project dialog

\* Add dialog to reload assignments of shared resources.  
This makes it possible to re-schedule projects  
in a different sequence than first time, or insert new projects  
between old projects.

\* Always save currency in case project is opened in a different locale.

\* Fix crash when creating new project with separate holiday calendar.

Help and Documentation:  
\* Help is provided using What's this (Shift-F1).  
Where needed, a link to the relevant page  
in the documentation is provided.

\* Documentation has been improved (still WIP).

Task editor:  
\* Add copy/paste of tasks, also possible using drag/drop.

\* BUG: 412749 - Allow to indent/unindent multiple tasks.

\* BUG: 412341 - add option to specify dependencies manually.  
Adds a dedicated dialog to task editor and dependency editor.  
Makes it possible to add predeccessors to the selected task.

Typical use case:  
1) Add a task  
2) Open the dependency dialog  
3) Add the dependency/dependencies (or just accept to connect to previous task)

By default a dependency on the previous task is presented.  
The task dropdown list is sorted with prevoious tasks first.

\* BUG: 412134 - Allow for entering estimates in minutes.

\* BUG: 309038 - Add priority feature, use value for levelling/scheduling.

\* Allow to drag/drop non-baselined tasks on a baselined project.

\* BUG: 406127 - Unable to delete tasks.  
Enable Delete action also when one task is selected and a different task is current task.

\* TaskModules: Improve task modules handling.  
Add posibility to store multiple groups of task modules,  
and add parameter substitution.

Dependency Editor:  
\* BUG: 401511 - Bug in Finish-Finish dependency check in dependency editor.

Scheduling:  
\* Add automatic scheduling mode.

\* Use task priority (Bug 309038).

\* Handle removed task/resource during scheduling.

\* Fix crash on close when scheduling is running.

Ganttview:  
\* Enable editing of completion.

\* BUG: 284361 - Extend the "View" menu (Zoom, Scale, Timeformat, Year Format, Grid) in Gantt view mode.

\* Add timeline.

\* Add contextmenu -> Show Unscheduled Tasks.

\* Trigger an update of the chart when project start may have changed.

\* Enable context menu from gantt chart.

Performance charts:  
\* Coordinate rubberband selection, drag&drop and context menus.

Reports:  
\* Improve report templates and generation.

\* Add documentation.

Filters:  
\* ICalExport filter: Enable user to choose what to export.

\* Add import filter for Gnome Planner project files.

Workpackage handling:  
\* Improve workpackage handling.  
Give project manager much more control of the merge process,  
both by giving control of what shall be updated as well as  
presenting the result and the posibility to make modifications  
before commiting.  
Also, the manager can now reject the package.

Also adds possibility to drag workpackage(s) onto the workpackage view,  
e.g. from a mail.

\* BUG: 287563 - Bug in merging workpackage used effort.  
Use "Per Resource" as default edit mode in the progress dialog.  
This mode must be used when progress is entered from planwork and  
makes it unnecessary for the project manager to change the mode manually.

\* BUG: 342574 - Can not edit a progresse entry of a task.  
Improve progress dialog in general.  
Main change is to put both "resouce entry" tab and "date entry" tab on the same page, which makes it easier to see the connection between them.  
The "resource entry" part is only shown when in "Per resource" edit mode.  
When effort is entered in "resource entry" part, a date entry is automatically added in the "date entry" part.  
Also there are now only two modes, "Per Resource" and "Per Task".  
"Per Task" includes the functionallity in the prevoius "Calculate Effort" mode.  
The default is now set to "Per Resource" as this preferred mode if PlanWork is used, so that this work by default.  
If the manager wants easier data entry, he can select "Per Task" edit mode.

Tooltips and What's this has been added to further enhance usability.

PlanWork:  
\* Enable opening docuemnts from gantt view.

\* Enable sorting gantt view.

\* Fix sorting by dattime.

\* Mark finished tasks with a checkmark.
