---
title: First Calligra Sprint
date: 2011-03-31
author: zagge
categories: []
---

Already tomorrow the first Calligra Sprint will start! There will be many, many people for whom it is their first sprint, but there will also be many old-time Calligra hackers gathering at the KDAB office in Berlin which is kindly provided to us.

All-in-all, there will be more than thirty Calligra developers! That means we more than doubled the number of participants compared to [the last sprint in Essen](http://dot.kde.org/2010/06/25/koffice-2010-summer-sprint-report). And not only developers will attend, Anna, an interaction design student of the University of Oulu will join us to present the results of usability testing that she and her colleagues have done on Calligra's user interface.

No doubt this will be one of the coolest, most productive sprints, and we will keep you posted with what's happening to Calligra in Berlin!

These face-to-face gatherings are incredibly valuable, and we are really grateful to KDE e.V. for making this gathering possible. It's already being a mini-conference... And that promises something great for our future!
