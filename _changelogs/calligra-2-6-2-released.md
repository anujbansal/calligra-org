---
title: Calligra 2.6.2 Released
date: 2013-03-13
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.6.2, the second bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.6.1 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes that are in 2.6.2. There are several others that are not mentioned here.

### General:

- Mime types were synced between the Words, Sheets and Stage applications and the Calligra Active application.

### Filters:

- A bugfix in the import filter for the old KSpread file format that led to styles being treated wrong.

### Sheets:

- Fix for a bug that sometimes made the error #CIRCLE appear on a cell even when there was no circular dependency (bug 316244)

### Krita:

- Fix for a bug with the OpenGL drivers that led to the user sometimes have to remove their kritarc file manually (bug 308713).
- Krita now correctly remembers the layer box status.
- A crash fix for empty layers
- A fix in the autosave code
- Fix for a bug when a layer was rotated counter clockwise.

### Kexi:

- Don't require write access in file connection for read-only mode

### Try It Out

- **The source code** is available for download: [calligra-2.6.2.tar.bz2](http://download.kde.org/stable/calligra-2.6.2/calligra-2.6.2.tar.bz2). As far as we are aware, the following distributions package Calligra 2.6. This information will be updated when we get more details. In addition, many distributions will package and ship Calligra 2.6 as part of their standard set of applications.
- In **Chakra Linux**, Calligra is the default office suite so you don't have to do anything at all to try out Calligra.  Chakra aims to be a [showcase Linux for the "Elegance of the Plasma Desktop"](http://www.chakra-project.org/) and other KDE software.
- Users of **Ubuntu and Kubuntu** are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:
    
    sudo add-apt-repository ppa:neon/ppa \\
    && sudo apt-get update\\
    && sudo apt-get install project-neon-base \\
       project-neon-calligra \\
       project-neon-calligra-dbg
    
    You can run these packages by adding /opt/project-neon/bin to your PATH.
- **Arch Linux** provides Calligra packages in the \[kde-unstable\] repository.
- **Fedora** packages are available in the rawhide development repository ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repository at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .
- There are **OpenSUSE** Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).
- Calligra **FreeBSD** ports are available in [Area51](http://freebsd.kde.org/area51.php).
- **MS Windows** packages are available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html).
- **Mac OS X:** We would welcome volunteers who want to build and publish packages for the Calligra Suite on OS X. There are some first attempts

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
