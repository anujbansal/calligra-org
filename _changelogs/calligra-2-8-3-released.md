---
title: Calligra 2.8.3 Released
date: 2014-05-15
author: jstaniek
categories: []
---

This is the third (and [last but one](http://community.kde.org/Calligra/Schedules/2.8/Release_Plan)) update to the 2.8 series of the [Calligra Suite, and Calligra Active](http://www.calligra.org/) released to fix recently found issues. The Calligra team recommends everybody to update.

## Issues Fixed in This Release

### General

- Add support for line breaks when reading the OpenDocument Format.
- Fix a bug in the style manager for filters.
- Make the text shape (specifically the dock panel) work on Windows 8.1.
- Excel document support: add support for 1904-based XLS files, typically created by Excel on Mac OS X.

### Kexi

- Fix crash when closing form design window and color property isn't saved. (bug 333551)
- Make context menu work for tables in Query Designer.
- Make it possible to set solid and inherited background color in form widgets. "Auto Fill Background" property has been added for this. It's set to true when user selects background color. (bugs 333548, 333549)
- Fixed crash in Debug GUI when trying to modify table field. (bug 333933)
- Updated list of SQL keywords reserved for KexiSQL, SQLite, MySQL and PostgreSQL.
- Make it possible to use reserved words as names for table columns. (bug 332160)
- Fix crashes when closing tabs or windows (bug 334234)
- Removed a number of resource leaks.

### Krita

- Translation fix in the Multihand tool: Axis -> Axes.
- More precise translations. (bug 333135)
- A fix for the outline of invert selection.
- Krita no longer closes immediately when a file is corrupted but gently warns the user.
- Fix bug: deleting Group-Layer and contents when there are no other layers prevented user from creating new layers (crash). (bug 333496)
- Improved detection of supported image formats.
- Removed a number of resource leaks.
- Add support for selection in GMIC filters. (bug 325771)
- Fix crash when GMIC filter is applied to the layer which was moved. (bug 327980)
- Add search box for filter names of the GMIC plug-in. A text box below the filters tree can now be used to find the GMIC filter by name.
- Select only paint layers when gathering all Krita layers from layer stack.
- Remember the last used preset across sessions.
- Fix invalid recalculation of width and height between units.
- Ensure that the channel flags are always reset when they are set to full, otherwise compositing will not be will work not efficient. (bug 333080)
- Fix painting grid on lower zoom levels. (bug 333234)
- Fix a triangular brush outline for a 1-pixel brush. (bug 334408)
- Fix pixel-alignment of the Rectangle and Ellipse tools, perform alignment exactly how the user expects. (bug 334508)
- Make layer actions such as "Delete the layer or mask" listed in the in Configure Shortcuts dialog. (bug 332367)
- Fix handling a tablet when "Pan/Scroll" mode is assigned to a button. Note: Wacom's "Pan/Scroll" feature supports only vertical wheel scroll, so using usual Middle-button panning is recommended. (bug 334204)

\[caption id="attachment\_4110" align="aligncenter" width="300"\][![GMIC filter search](/assets/img/sm_w19_gmic-search.jpg)](http://www.calligra.org/wp-content/uploads/2014/05/sm_w19_gmic-search.jpg) GMIC filter search in Krita\[/caption\]

### Calligra Words

- Export to text: fix missing number of spaces in the text:s tag.
- Export to text: fix missing newlines after a heading.
- Export to text: add support for line breaks.

### Try It Out

- **The source code** is available for download: [calligra-2.8.3.tar.xz](http://download.kde.org/stable/calligra-2.8.3/calligra-2.8.3.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## About Calligra

Calligra Suite is a graphic art and office suite. It is a part of the applications family from the KDE community. See more information at the website [http://www.calligra.org](http://www.calligra.org/).
