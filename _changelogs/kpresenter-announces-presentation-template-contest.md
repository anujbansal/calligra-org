---
title: KPresenter Announces Presentation Template Contest
date: 2010-07-23
author: Inge Wallin
categories: []
---

[KPresenter](http://www.koffice.org/kpresenter/) is making great progress on the way to KOffice 2.3.

But we are still lacking good templates for creating new presentations. That is where we need your help and that is why we are happy to announce the [_KPresenter template contest_](http://www.koffice.org/kpresenter/template-contest/ "The contest page"). Create new templates for cool presentations, be it KDE themed, Free Software themed, business themed or school project themed -- or whatever tickles your fancy, and your work could be part of the next release of KPresenter. So sharpen your digital pens and get started!

As noted above, see the [contest page](http://www.koffice.org/kpresenter/template-contest/ "The contest page") for all the details.
