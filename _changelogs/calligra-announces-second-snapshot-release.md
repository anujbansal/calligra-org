---
title: Calligra Announces Second Snapshot Release
date: 2011-06-16
author: boudewijn
categories: []
---

Today, the Calligra project announces the second snapshot release of its suite of productivity and creativity applications. While we are working towards our first end-user release, expected in October, we want to share with everyone who is interested in the project the progress we make from month to month.

And there's plenty to investigate! Let's have a look at all the improvements made since [the last snapshot](http://www.calligra-suite.org/news/calligra-announces-first-snapshot-release/):

### Words

While most work has gone into the layout engine, some user interface fixes have started to appear. Notably it has become possible to manage page styles in the page layout dialog. Following on the integrated find functionality in the last snapshot, we find another nice UI improvement in the statusbar area: a quick pagelayout access button and goto page/line field.

The new layout engine introduced in the first snapshot was still very immature. An immense amount of work has gone into maturing the layout engine. We're now at a stage where we need as much real-world testing as possible! Of course, we have a huge repository with real documents we test with, but those will never cover all bases.

So just try out your favorite text documents, be it .odt, .doc or .docx format, and you will see how much closer we have come to perfect display. One improvement that will be very noticeable is how much better we handle anchored and inline images -- and if you find a problem, contact the Words team on #calligra on irc.freenode.net, the [calligra-devel mailing list](https://mail.kde.org/mailman/listinfo/calligra-devel), the [Calligra Forums](http://forum.kde.org/viewforum.php?f=203) or directly in [bugs.kde.org](http://bugs.kde.org).

And it's not just the layout engine that has been improved a lot: Words has again improved a lot in its ability to load Microsoft Word documents, both .doc and .docx and .odt. For .docx we are far better than say LibreOffice already, and for .doc we are getting very close to being the best out there too.

And in the future... The next snapshot will contain support for creating Table-of-Contents sections that will always be up to date -- Words will keep it up to date automatically with your content, instead of waiting for you to ask Words to regenerate. And there will be many other cool developments -- Words is moving fast towards being ready for the ordinary user.

### Krita

This snapshot includes many of the results of the [Krita sprint](http://dot.kde.org/2011/06/02/what-happens-when-artists-and-developers-come-together-2011-krita-sprint). While Krita 2.3.3 remains the stable release, users are recommended to use this snapshot instead if they want to benefit from the many usability and stability improvements.

- painting is smoother than ever, thanks to Geoffry Song's work.
- printing to pdf has been improved thanks to Sven Langkamp, who also fixed a host of smaller bugs
- the preset editor now has a preset selector embedded, making it even easier to create new presets, thanks to the work by Pentalis
- improved and configurable full-screen/just the canvas mode
- opacity can now be part of the paintop, thanks to Silvio Heinrich
- improved compatibility with some jpg files, thanks to Cyrille Berger
- _first gsoc result_: Get Hot New Stuff integration for resources like patterns, gradients and brushes, by Srikanth Tiyyagura
- new shortcuts to dynamically change saturation and value for the current brush, by Lukas Tvrdy -- K = darker, L = lighter,
- much clearer view of currently selected color in the advanced color selector by Adam Celarek.

\[caption id="attachment\_2098" align="aligncenter" width="500" caption="Krita's new preset editor with quick-selection strip"\][![](/assets/img/krita_preset_editor-500x243.jpg "Krita Preset Editor")](http://www.calligra-suite.org/news/calligra-announces-second-snapshot-release/attachment/krita_preset_editor/)\[/caption\]

### Kexi

The Kexi team has made Kexi start up much, much faster, even with largish database files. See [delayed\_plugin\_loading\_benchmark.ods](http://www.kexi-project.org/download/delayed_plugin_loading_benchmark.ods) for more details -- and for kicks, check it out with Calligra Tables!

### Calligra Active

Calligra Active is the community project to adapt the Calligra engine to touch interfaces and fits in [Plasma Active project](http://aseigo.blogspot.com/2011/04/plasma-active-calligra-active.html). It uses QML and so integrates with MeeGo. In this release, Calligra Active can, for the first time, be used to view text documents, spreadsheets and presentations.

### Others

The project planning application Plan is now much better at producing reports in ODT format, Braindump is much stabler (but still is a very young application, so help us find more problems!), Karbon's artistic text tool is much more powerful and easy to use, the support for the OpenDocument file format has improved again, Calligra Tables has improved compatibility with Excel. And so on... Check it out for yourself!

### Get It

The source code of the snapshot is available for download: [calligra-2.3.72.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.72/calligra-2.3.72.tar.bz2)

#### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

#### Arch Linux

Arch Linux provides Calligra packages in the kde-dev repository.

#### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

#### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).
