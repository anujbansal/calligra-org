---
title: Calligra Words Receives Akademy Award For Best Application
date: 2012-07-02
author: Inge Wallin
categories: [uncategorized]
---

Yesterday Calligra Words and its maintainer Camilla Boemann [received the award for Best KDE Application](http://blogs.kde.org/node/4584) at the Akademy award ceremony. The motivation for the award mentioned that they wanted to give it to the whole suite but they had to pick one application and one person.

Akademy is the yearly community conference for the KDE community. The jury consisted of the winners from last year.

The whole Calligra team should feel as receiver of this award and is very proud to be recognized in this way.  This will for sure motivate our future work.
