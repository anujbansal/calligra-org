---
title: Calligra 2.7.3 Released
date: 2013-10-11
author: jstaniek
categories: []
---

Quite long ago the Calligra team has released a bugfix version 2.7.3 of the [Calligra Suite, and Calligra Active](http://www.calligra.org/). This is the formal announcement, we're sorry for the delay. The release contains a important bug fixes to 2.7.2 and we recommend everybody to update.

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that are not mentioned here.

### General:

- Many fixes for Mac OS X version. Kexi on Mac has been temporarily disabled.

### Krita:

- Fix incremental backup (F4 key), files were overwritten. (#314214)

### Kexi:

- Fix sorting error in table views. The sorting was broken when NULL values are compared with text for text columns. (#324224)
- Fix escaping of text values when working with PostgreSQL. Properly escape the ' character. (#318129)
- Improved ease of use when inserting new fields in Table Designer. (#323827)
- Fix bug introduced in Kexi 2.7: Table Designer does not display properties. (#323804)

### Try It Out

- **The source code** is available for download: [calligra-2.7.3.tar.xz](http://download.kde.org/stable/calligra-2.7.3/calligra-2.7.3.tar.xz).
- **Binary packages.** Please check your distribution for the new binary packages, in the same place where previous versions of packages were published. This information will be updated at [http://userbase.kde.org/Calligra/Download](http://userbase.kde.org/Calligra/Download) when we get more details. We will be grateful for any feedback (users can update that wiki page on their own).

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
