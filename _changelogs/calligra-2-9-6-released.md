---
title: Calligra 2.9.6 Released
date: 2015-07-10
author: jstaniek
categories: []
---

We are pleased to announce that [Calligra Suite, and Calligra Active 2.9.6](http://www.calligra.org) have just been released. This recommended update brings further improvements to the 2.9 series of the applications and underlying development frameworks.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-6-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Fix building on CentOS and Windows
- Fix Collapse button in docker titlebar shown even if set to uncollapsable
- Fix saving of sections inside tables
- Fix "keep lines together" check box in paragraph check box
- Restore loading of default styles (ODF's defaultstyles.xml) and make it work when multiple document types are used in the very same application (for example in Gemini).
- Fix disappearing blocks during relayouts. Symptoms would be that the block would appear and reappear depending on both pages being layouted or not (bug 345621)
- Make sure the cursor is correct when hovering the corner handles of a shape (bug 347843)
- Fix preset docker flickering and slowdown (related to bug 344968)
- Make sure the footnote styles are found and that the footnote number (in the footnote itself) is present in the right baseline (using the correct footnote style when creating a new footnote is a bit problematic though, LibreOffice doesn't see to actually save the style) (bug 323232)
- Don't delete when at the end of table cell (bug 346297)
- Fix an issue where some config files may not be picked up

### Kexi

- _General_:
    - Fix renames for file storing the Welcome status bar's GUIs
    - Recent Projects: use file's base name as a good replacement for caption when caption is not available
    - Fix left margin for the global search box (dependent on style); also react on changing widget style
    - Fix possible crash caused by command line arguments passed to Kexi in a wrong way
    - Fix crash appearing when the --hide-menu command line option is used
- _Queries_:
    - Fix possible crash in result handling of queries
- _SQLite databases_:
    - Fix compacting databases (properly rename files back to the original name)
- _PostgreSQL databases_:
    - Fix crash when importing a PostgreSQL database to a .kexi file (bug 349156)

### Krita

- _New Features_:
    - Add possibility to continue a Crop Tool action
    - Speed up of color balance, desaturate, dodge, hsv adjustment, index color per-channel and posterize filters.
    - Activate Cut/Copy Sharp actions in the menu
    - Implemented continuation of the transform with clicking on canvas
    - new default workspace
    - Add new shortcuts ('\\' opens the tool options, f5 opens the brush editor, f7 opens the preset selector.)
    - Show the tool options in a popup (toggle this on or off in the general preferences, needs restarting Krita)
    - Add three new default shortcuts (Create group layer = Ctrl+G, Merge Selected layer = Ctrl+Alt+E, Scale image to new size = Alt+Ctrl+I )
    - Add an 'hide pop-up on mouseclick option' to advanced color selector.
    - Make brush 'speed' sensor work properly
    - Allow preview for "Image Background Color and Transparency" dialog.
    - Selection modifier patch is finally in! (shift=add, alt=subtract, shift+alt=intersect, ctrl=replace. Path tool doesn't work yet, and they can't be configured yet)
- _Bug fixes_:
    - Fix crash when saving a pattern to a \*.kra (bug 346932)
    - Make Group Layer return correct extent and exact bounds when in pass-through mode
    - Make fixes to pass-through mode.
    - Added an optional optimization to slider spin box
    - Fix node activating on the wrong image (bug 348599)
    - Fix deleting a color in the palette docker (bug 349792)
    - Fix scale to image size while adding a file layer (bug 349823)
    - Fix wrapping issue for all dial widgets in Layer Styles dialog
    - Fix calculation of y-res when loading .kra files
    - Prevent a divide by zero (bug 349598)
    - Reset cursor when canvas is extended to avoid cursor getting stuck in "pointing hand" mode (bug 347800)
    - Fix tool options visibility by default (bug 348730)
    - Fix issue where changing theme doesn't update user config (bug 349446)
    - Fix internal brush name of LJF smoke (bug 348451)
    - Set documents created from clipboard to modified (bug 349424)
    - Make more robust: check pointers before use (bug 349451)
    - Use our own code to save the merged image for kra and ora (is faster)
    - Fix Hairy brush not to paint black over transparent pixels in Soak Ink mode (bug 313296)
    - Fix PVS warning in hairy brush
    - Don't limit the allowed dock areas (bug 348750)
    - Fix uninitialized m\_maxPresets (bug 348795)
    - (gmic) Try to workaround the problem with busy cursor
    - (gmic) If there is selection, do not synchronize image size (bug 349346)
    - Disable autoscroll for the fill-tool as well (bug 348887)
    - Rename the fill layers (bug 348914)

### Calligra Plan

- Restore option to add items to reports in Plan's report designer
- Fix and update background app icon in Plan about HTML view

### Calligra Words

- Fix crash appearing sometimes with page breaks, and make page breaks work correctly on multi-column pages
- Handle column breaks
- Fix regressions that made floating text shapes geometry protected (bug 345426)
- Fix a crash on closing a second document (bug 336145)

  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.6.tar.xz](http://download.kde.org/stable/calligra-2.9.6/calligra-2.9.6.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.6/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.6/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
