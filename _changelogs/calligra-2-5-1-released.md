---
title: Calligra 2.5.1 Released
date: 2012-08-29
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.5.1, the first bugfix release of the [Calligra Suite,  and Calligra Active](http://www.calligra.org/). This release contains a number of important bug fixes to 2.5.0 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

Here is an overview of the most important fixes are in 2.5.1. There are several others that are not mentioned here:

### Words:

- Fix: crash when saving file with imported text (bug 298899)
- Do not save with an attribue that makes LibreOffice and OpenOffice crash (BUG: 298689 )

### Kexi:

- Workaround for visual glitch of Oxygen style for the Modern Menu widget in kexi (bug 305051)
- Fix crash on importing CSV files with more than 1024 columns (bug 304405) and with Mac line endings (bug 304329)
- Missing table kexi\_\_parts is no longer a critical error in Kexi (this enables opening databases created e.g. by the future Words Bibliography tool)
- Fixed bug: New Kexi project file created in current directory instead of the selected one (bug 305163)
- Table View: Fixed misplaced 'Date/Time data error' validation popup in tableview (bug 282295)
- Table View: Show warning when invalid date values in tables and forms are rejected (bug 299867)
- Table View: Clicking validation popup editor is focus again allowing to correct the value
- Table View: Use new message widget for validation popups
- PostgreSQL support: Display cmake compile warning if libpqxx is not in version 3.x
- buildsystem: move checks for pgsql, mysql, tds, xbase dependencies to global area (bug 300871)

### Krita:

- Make it possible to open Krita 2.5 documents in 2.4 if no composition was set.
- Fix: conversion of HDR colorspaces
- Fix: crash when merging down the top layer if there is a selection (bug 305465)

### Common:

- Fix: Uncompressed ODF is saved in wrong directory (bug 305794)
- Don't mirror an image with a width of one pixel or less (bug 166324)

## Try It Out

The source code of this version is available for download here: [calligra-2.5.1.tar.bz2](http://download.kde.org/stable/calligra-2.5.1/calligra-2.5.1.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html). There is also a separate package or [Krita](http://www.krita.org/) only.

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
