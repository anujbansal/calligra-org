---
layout: page
title: Download
sorted: 3

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        !SITE_TITLE! is already available on majority of Linux distriutions. You
        can install it from <a href="http://userbase.kde.org/Calligra/Download#Linux">here</a>.
  - name: FreeBSD
    icon: /assets/img/freebsd.svg
    description: >
        !SITE_TITLE! is available on FreeBSD. You
        can install it from <a href="http://userbase.kde.org/Calligra/Download#FreeBSD">here</a>.
  - name: Windows
    description: >
        !SITE_TITLE! is available on Windows but with preliminary support. You
        can install it from <a href="http://userbase.kde.org/Calligra/Download#MS_Windows">here</a>.
  - name: MacOS
    description: >
        !SITE_TITLE! is available on MacOS but with preliminary support. You
        can install it from <a href="http://userbase.kde.org/Calligra/Download#Mac_OS_X">here</a>.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        You can download Calligra source code so you can 
        <a href="http://community.kde.org/Calligra/Building"> build applications on your own </a>. If you want to build !SITE_TITLE! from sources, we recommend checking our
        <a href="get-involved.html">Getting Involved</a> page which contains
        links to full guide how to compile !SITE_TITLE!.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        !SITE_TITLE! git repository can be viewed
        <a href="!SITE_GIT!">using cgit</a>.

        To clone !SITE_TITLE! uses <code>git clone !SITE_GIT!</code>. for
        detailed instructions how to build !SITE_TITLE! from source, check
        the <a href="get-involved.html">Getting Involved page</a>
---

<h1>Download</h1>

<div class="distribution-table">
{% for source in page.sources %}
<div class="d-flex mt-4">
  <div style="min-width: 80px; max-width: 80px;" class="mr-3">
   {% if source.icon %}
     <img
       alt="{{ source.name }} icon"
       class="img-fluid w-100"
       src="{{ source.icon }}"
     />
   {% endif %}
  </div>
  <div>
    <h2>{{ source.name }}</h2>
    {{ source.description | markdownify | replace: '!SITE_TITLE!', site.title | replace: '!SITE_GIT!', site.git }}
  </div>
</div>
{% endfor %}
</div>
