---
layout: component
name: Karbon
screenshot: /assets/img/karbon-summary.png
shortDescription: >
    Create clipart, logos, illustrations or photorealistic vector images
description: >
    Karbon is a vector drawing application with an user interface that is easy to use, highly customizable and extensible. That makes Karbon a great application for users starting to explore the world of vector graphics as well as for artists wanting to create breathtaking vector art.
css-include: /css/component.css
order: 4
redirect_from: 
- /karbon/screenshots/
- /karbon/features/
---

Karbon is a vector drawing application with an user interface that is easy to use, highly customizable and extensible. That makes Karbon a great application for users starting to explore the world of vector graphics as well as for artists wanting to create breathtaking vector art.

Whether you want to create clipart, logos, illustrations or photorealistic vector images – look no further, Karbon is the tool for you!

Karbon is free software and is getting better every day. To increase the pace at which we are moving forward you are invited to contribute in the form of code, documentation, tutorials, document templates or user feedback. Join us on #calligra at irc.freenode.net, or on our [mailinglist](https://mail.kde.org/mailman/listinfo/calligra-devel).

## Features
* Loading support for ODG, SVG, WPG, WMF, EPS/PS
* Writing support for ODG, SVG, PNG, PDF, WMF
* Customizable user interface with freely placable toolbars and dockers
* Layer docker for easy handling of complex documents including preview thumbnails, support for grouping shapes via drag and drop, controlling visibility of shapes or locking
* Advanced path editing tool with great on-canvas editing capabilies
* Various drawing tools for creating path shapes including a draw path tool, a pencil tool as well as a calligraphy drawing tool
* Gradient and pattern tools for easy on-canvas editing of gradient and pattern styles
* Top notch snapping facilities for guided drawing and editing (e.g. snapping to grid, guide lines, path nodes, bounding boxes, orthogonal positions, intersections of path shapes or extensions of lines and paths)
* Includes many predefined shapes including basic shapes like stars, circle/ellipse, rectangle, image
* Artistic text shape with support for following path outlines (i.e. text on path)
* Complex path operations and effects like boolean set operations, path flattening, rounding and refining as well as whirl/pinch effects
* Extensible by writing plugins for new tools, shapes and dockers

## Screenshots

[![Karbon- gearflowers](/assets/img/karbon-complex.png)](/assets/img/karbon-complex.png "Karbon- gearflowers")

<center> Complex SVG images can be loaded, displayed and manipulated without problems. </center> <br/> 

[![Karbon-connections](/assets/img/karbon-shapes.png)](/assets/img/karbon-shapes.png "Karbon-connections")

<center>Karbon has support for connections between shapes as well as support for text on path. </center> <br/>



