---
layout: component
name: Old Components
css-include: /css/component.css
order: 8
redirect_from: 
- /braindump/
- /flow/
- /krita/
---
These are some of the older components of Calligra Suite. These components are no longer a part of Calligra. They are now either standalone apps or unmaintained.

## Braindump

Braindump is an open source tool to dump and organize the content of your brain (ideas, drawings, images, texts…) to your computer.

[![Braindump Screenshot](/assets/img/braindump.png)](/assets/img/braindump.png "Braindump")

Braindump is now unmaintained

### Features
* Whiteboards where you can write text, add images, music sheet, chart, formula...
* Vectorial drawing
* Copy/paste support with ODF able applications

## Author

Calligra Author helps you develop your eBook document from concept to completion. Calligra Author is the one-stop tool for creating ebooks. It supports the user in all the phases of writing: planning, writing, review and publishing. Calligra Author is especially aimed at novelists and textbook writers.

[![Calligra Author Screenshot](/assets/img/author-2.8-comments.png)](/assets/img/author-2.8-comments.png "Calligra Author")

Author is now unmaintained

### Features

* Templates.
* Themes.
* Toolbars and Dockers.
* Full screen mode.
* Zoom in/out.
* Reference objects: People, Events, Locations.
* Semantic stylesheets.
* Style Manager: Paragraphs, Characters.
* Auto spell check.
* Autocorrection.
* Export: OpenDocument Text, OpenDocument Text (Uncompressed XML Files), OpenDocument Text * (Encrypted), OpenDocument Text Template, Mobipocket e-book, HTML document, electronic book * document.
* Export as PDF.
* Improved text statistics (word count, character count, etc).
* Access Google Documents.

## Flow

Calligra Flow is an easy to use diagramming and flowcharting application with tight integration to the other Calligra applications. It enables you to create network diagrams, organisation charts, flowcharts and more.

[![Calligra Flow Screenshot](/assets/img/calligra-flow.png)](/assets/img/calligra-flow.png "Calligra Flow")

Flow will be merged with Karbon

### Features

* Create network diagrams, organisation charts, flowcharts and more
* Scriptable stencil creation using Python
* Support for Dia stencils.
* Plugin framework for adding more functionality.

## Krita

[Krita](https://krita.org) is the full-featured digital art studio.

It is perfect for sketching and painting, and presents an end–to–end solution for creating digital painting files from scratch by masters.

Krita is a great choice for creating concept art, comics, textures for rendering and matte paintings. Krita supports many colourspaces like RGB and CMYK at 8 and 16 bits integer channels, as well as 16 and 32 bits floating point channels.

Have fun painting with the advanced brush engines, amazing filters and many handy features that make Krita enormously productive.

[![Krita Screenshot](/assets/img/krita_2.9_texture.jpg)](/assets/img/krita_2.9_texture.jpg "Krita")

Krita was part of Calligra up to version 2.9 and became a project independent from Calligra since then.

