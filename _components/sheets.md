---
layout: component
name: Sheets
screenshot: /assets/img/sheets-summary.png
shortDescription: >
    Create spreadsheets with formulas and charts
description: >
    Calligra Sheets is a fully-featured spreadsheet application. Use it to quickly create spreadsheets with formulas and charts, to calculate and organize your data.
css-include: /css/component.css
order: 2
redirect_from: /sheets/screenshots/
---

Calligra Sheets is a fully-featured calculation and spreadsheet tool. Use it to quickly create and calculate various business-related spreadsheets, such as income and expenditure, employee working hours...

# Features

## Create to show

Calligra Sheets benefits from the same technology all other Calligra products do, which means you can do more. Use shapes for notes or create flowcharts and mindmaps while you work – the tools are as easy to use as in any other Calligra application. Explore!

## Carefully chosen templates

Benefit from our default templates for the most common use-cases. Instantly create balance-sheets and invoices or credit-card trackers and vacation lists, so you spend more time on what’s really important.

[![Sheets Templates](/assets/img/sheets-templates.png)](/assets/img/sheets-templates.png "large range of pre-defined templates")

## Powerful and comprehensive formula list

Automate formula creation with the comprehensive formula list – be it economic or scientific, or anything else. Never struggle to remember a formula again.

[![Sheets Formula List](/assets/img/sheets-formula.png)](/assets/img/sheets-formula.png "Comprehensive formula list")

## What you need – much, much faster

Work in a familiar environment and use the same tools and functions as you’re used to – but in a much more responsive application. Open files, edit, save and close them faster then ever before. Do more!

[![Sheets Familiar UI](/assets/img/sheets-templates.png)](/assets/img/sheets-familiar.png "Work in a familiar environment")